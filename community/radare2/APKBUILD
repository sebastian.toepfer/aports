# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: stef <l0ls0fo2i@ctrlc.hu>
# Contributor: Jose-Luis Rivas <ghostbar@riseup.net>
# Maintainer: Valery Kartel <valery.kartel@gmail.com>
pkgname=radare2
pkgver=5.6.4
pkgrel=0
pkgdesc="An opensource, crossplatform reverse engineering framework"
url="http://www.radare.org"
arch="all"
license="GPL-3.0"
options="net !check" # upstream does not provide any working testsuite
makedepends="$depends_dev libzip-dev openssl1.1-compat-dev capstone-dev linux-headers"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-libs"
source="$pkgname-$pkgver.tar.gz::https://github.com/radare/radare2/archive/$pkgver.tar.gz
	string-header-build-fix.patch"

# secfixes:
#   4.5.0-r0:
#     - CVE-2020-15121
#   3.9.0-r0:
#     - CVE-2019-14745
#     - CVE-2019-12865
#     - CVE-2019-12829
#     - CVE-2019-12802
#     - CVE-2019-12790

build() {
	[ "$CARCH" = "s390x" ] && _disable_debugger="--disable-debugger"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		$_disable_debugger \
		--with-syscapstone \
		--with-openssl \
		--with-syszip
	make HAVE_LIBVERSION=1
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
5e7ff9c5d73685a4a0167750f74bb7ddcb19d3e3d2a306d8eea00b4f961efdbed4843f6e3ff66ef7108a91d372fc403eaf7343a3f4d1bada4e5ac243529e2593  radare2-5.6.4.tar.gz
5e60a1112a10cdd6a31374ba9b564d6522a37086380cf74b7232e32fb700ae1b2f68edbc0726ebedcc31d4789bc4f89525f117f25dac3609c60a75d471e49c85  string-header-build-fix.patch
"
