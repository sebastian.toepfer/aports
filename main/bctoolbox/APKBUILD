# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=bctoolbox
pkgver=5.1.10
pkgrel=0
pkgdesc="Utilities library used by Belledonne Communications softwares like belle-sip, mediastreamer2 and linphone"
url="https://github.com/BelledonneCommunications/bctoolbox"
arch="all"
license="GPL-2.0-or-later"
options="!check" # bcunit not available
makedepends="cmake mbedtls-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/BelledonneCommunications/bctoolbox/archive/$pkgver.tar.gz"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_MBEDTLS=YES \
		-DENABLE_POLARSSL=NO \
		-DENABLE_STATIC=NO \
		-DENABLE_TESTS_COMPONENT=OFF \
		-DENABLE_SHARED=YES .
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/cmake/bctoolbox
	mv "$pkgdir"/usr/share/bctoolbox/cmake/* "$subpkgdir"/usr/lib/cmake/bctoolbox
	# Remove empty dirs
	rmdir "$pkgdir"/usr/share/bctoolbox/cmake
	rmdir "$pkgdir"/usr/share/bctoolbox
	rmdir "$pkgdir"/usr/share
}

sha512sums="
296188de6f71b4cf005f2b95599304e4b37e066deac9fb77128d4b4f0f029d1b3e76b8458860b33d87ceba4f8774b5ba7a69b1b408233f2690a91f70d3efdbc4  bctoolbox-5.1.10.tar.gz
"
